﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        bool hasClick;
        public Form1()
        {
            InitializeComponent();
            hasClick = false;
            Error_box.Visible = false;
        }

        private void Ok_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Details_button_Click(object sender, EventArgs e)
        {


            if (this.hasClick == false)
            {
                this.Size = new System.Drawing.Size(340, 400);

                Error_box.Location = new Point(10, 170);
                Error_box.Visible = true;




                this.hasClick = true;
            }
            else
            {
                Error_box.Visible = false;
                this.Size = new System.Drawing.Size(340, 200);
                this.hasClick = false;
            }
 
        }

        private void Cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
