﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Fernando Alonso",
            "Ferrari",
            "Ferrari",
            "194"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "Sebastian Vettel",
            "Red Bull Racing ",
            "Renault",
            "165"}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "Kimi Raikkonnen",
            "Lotus F1 Team",
            "Renault",
            "149"}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Lewis Hamilton",
            "McLaren ",
            "Mercedes",
            "142"}, -1);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Mark Webber",
            "Red Bull Racing",
            "Renault",
            "132"}, -1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "Jenson Button",
            "McLaren",
            "Mercedes",
            "119"}, -1);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "Nico Rosberg",
            "Mercedes",
            "Mercedes",
            "93"}, -1);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "Romain Grosjean",
            "Lotus F1 Team",
            "Renault",
            "82"}, -1);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {
            "Sergio Perez",
            "Sauber",
            "Ferrari",
            "66"}, -1);
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem(new string[] {
            "Fellipe Masse",
            "Ferrari",
            "Ferrari",
            "51"}, -1);
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem(new string[] {
            "Paul Di Resta",
            "Force India",
            "Mercedes",
            "44"}, -1);
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem(new string[] {
            "Michael Schumacher",
            "Mercedes",
            "Mercedes",
            "43"}, -1);
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem(new string[] {
            "Kamui Kobayashi",
            "Sauber",
            "Ferrari",
            "35"}, -1);
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem(new string[] {
            "Nico Hulkenberg",
            "Force India",
            "Mercedes",
            "31"}, -1);
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem(new string[] {
            "Pastor Maldonado",
            "Williams",
            "Renault",
            "29"}, -1);
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem(new string[] {
            "Bruno Senna",
            "Williams",
            "Renault",
            "25"}, -1);
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem(new string[] {
            "Jean-Eric Vergne",
            "Toro Rosso",
            "Ferrari",
            "8"}, -1);
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem(new string[] {
            "Daniel Ricciardo",
            "Toro Rosso",
            "Ferrari",
            "6"}, -1);
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem(new string[] {
            "Timo Glock",
            "Marusia",
            "Cosworth",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem(new string[] {
            "Heikki Kovalainen",
            "Cateram",
            "Renault",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem21 = new System.Windows.Forms.ListViewItem(new string[] {
            "Vitaly Petrov",
            "Cateram",
            "Renault",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem22 = new System.Windows.Forms.ListViewItem(new string[] {
            "Jerome D\'Ambrosio",
            "Lotus F1 Team",
            "Renault",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem23 = new System.Windows.Forms.ListViewItem(new string[] {
            "Charles\' Pic",
            "Marusia",
            "Cosworth",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem24 = new System.Windows.Forms.ListViewItem(new string[] {
            "Narain Karthikeyan",
            "HRT",
            "Cosworth",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem25 = new System.Windows.Forms.ListViewItem(new string[] {
            "Pedro De La Rosa",
            "HRT",
            "Cosworth",
            "0"}, -1);
            this.listView1 = new System.Windows.Forms.ListView();
            this.DriverName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.EngineManufacturer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TeamName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Points = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.AllowColumnReorder = true;
            this.listView1.AllowDrop = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.DriverName,
            this.TeamName,
            this.EngineManufacturer,
            this.Points});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
            listViewItem1.Checked = true;
            listViewItem1.StateImageIndex = 1;
            listViewItem1.Tag = "FernandoAlonso";
            listViewItem1.ToolTipText = "FernandoAlonso";
            listViewItem2.Tag = "SebastianVettel";
            listViewItem2.ToolTipText = "SebastianVettel";
            listViewItem3.Tag = "Vodka";
            this.listView1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17,
            listViewItem18,
            listViewItem19,
            listViewItem20,
            listViewItem21,
            listViewItem22,
            listViewItem23,
            listViewItem24,
            listViewItem25});
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(485, 554);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // DriverName
            // 
            this.DriverName.Text = "Driver Name";
            this.DriverName.Width = 108;
            // 
            // EngineManufacturer
            // 
            this.EngineManufacturer.DisplayIndex = 1;
            this.EngineManufacturer.Text = "Engine manufacturer";
            this.EngineManufacturer.Width = 135;
            // 
            // TeamName
            // 
            this.TeamName.DisplayIndex = 2;
            this.TeamName.Text = "Team Name";
            this.TeamName.Width = 96;
            // 
            // Points
            // 
            this.Points.Text = "# of points in 2012 season";
            this.Points.Width = 141;
            // 
            // Form1
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(485, 463);
            this.Controls.Add(this.listView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader DriverName;
        private System.Windows.Forms.ColumnHeader TeamName;
        private System.Windows.Forms.ColumnHeader EngineManufacturer;
        private System.Windows.Forms.ColumnHeader Points;

    }
}

